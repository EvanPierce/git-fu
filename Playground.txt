This is an initial file for this playground project.
Tasks:
1) Pull this code down locally.
2) Create a new branch with changes.  Push new branch to repo.
3) Open a Pull Request from the new branch to the master branch. Merge it in.
4) Pull down the changes to master locally.
5) Change the name of the remote from origin to something new
6) Create a brand new directory and repeat tasks 1 - 5
7) Using these two copies of the repo, generate a merge conflict
8) Resolve the merge conflict on a rebase, open a Pull Request for the change to master and merge it in.
9) Blow away both directories, and repeat steps 1 - 8 in a new fork.
10) From the new fork, merge to the main repo's master a new file which explains the following git things:
a) fork vs. clone
b) rebase vs. merge
c) remote vs. local
d) branch vs. repo vs. project
e) reset --soft vs. reset --hard
f) git log
g) git add . vs. git add Playground.txt
h) git status vs. git diff vs. git diff --staged
i) pull vs. fetch
j) git stash
